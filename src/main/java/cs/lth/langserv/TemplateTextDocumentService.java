package cs.lth.langserv;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.eclipse.lsp4j.CodeActionParams;
import org.eclipse.lsp4j.CodeLens;
import org.eclipse.lsp4j.CodeLensParams;
import org.eclipse.lsp4j.Command;
import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionList;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.DiagnosticSeverity;
import org.eclipse.lsp4j.DidChangeTextDocumentParams;
import org.eclipse.lsp4j.DidCloseTextDocumentParams;
import org.eclipse.lsp4j.DidOpenTextDocumentParams;
import org.eclipse.lsp4j.DidSaveTextDocumentParams;
import org.eclipse.lsp4j.DocumentFormattingParams;
import org.eclipse.lsp4j.DocumentHighlight;
import org.eclipse.lsp4j.DocumentOnTypeFormattingParams;
import org.eclipse.lsp4j.DocumentRangeFormattingParams;
import org.eclipse.lsp4j.DocumentSymbolParams;
import org.eclipse.lsp4j.Hover;
import org.eclipse.lsp4j.Location;
import org.eclipse.lsp4j.MarkedString;
import org.eclipse.lsp4j.MessageParams;
import org.eclipse.lsp4j.MessageType;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.PublishDiagnosticsParams;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.ReferenceParams;
import org.eclipse.lsp4j.RenameParams;
import org.eclipse.lsp4j.SignatureHelp;
import org.eclipse.lsp4j.SymbolInformation;
import org.eclipse.lsp4j.SymbolKind;
import org.eclipse.lsp4j.TextDocumentPositionParams;
import org.eclipse.lsp4j.TextEdit;
import org.eclipse.lsp4j.WorkspaceEdit;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.lsp4j.services.TextDocumentService;

import cs.lth.langserv.DocumentModel.*;
import cs.lth.langserv.EclipseConMap;
import org.extendj.ast.Problem;

public class TemplateTextDocumentService implements TextDocumentService {
    private final Map<String, DocumentModel> docs = Collections.synchronizedMap(new HashMap<>());
    private final JavaLanguageServer javaLanguageServer;

    public TemplateTextDocumentService(JavaLanguageServer javaLanguageServer) {
        this.javaLanguageServer = javaLanguageServer;
    }

    @Override
    public CompletableFuture<Either<List<CompletionItem>, CompletionList>> completion(
            TextDocumentPositionParams position) {
        List<CompletionItem> completionItems = new ArrayList<>();
        completionItems.add(new CompletionItem("These"));

        completionItems.add(new CompletionItem("Are"));

        completionItems.add(new CompletionItem("From"));

        completionItems.add(new CompletionItem("My"));

        completionItems.add(new CompletionItem("languageserver"));

        completionItems.add(new CompletionItem("THIS SHOULD NOT BE AUTO"));

        completionItems.add(new CompletionItem("monkeybar"));
        return CompletableFuture.completedFuture(Either.forLeft(completionItems));
    }

    @Override
    public CompletableFuture<CompletionItem> resolveCompletionItem(CompletionItem unresolved) {
        return null;
    }

    @Override
    public CompletableFuture<Hover> hover(TextDocumentPositionParams position) {
        return CompletableFuture.supplyAsync(() -> {
                   DocumentModel doc = docs.get(position.getTextDocument().getUri());
                   String test = doc.getVariable(position.getPosition().getLine(), position.getPosition().getCharacter());
                   Hover res = new Hover();
                   List<Either<String, MarkedString>> dummy = new ArrayList<>();
                   dummy.add(Either.forLeft("Position: " + position.getPosition().toString()));
                   dummy.add(Either.forLeft(test));
                   res.setContents(dummy);
                   return res;
                });
    }

    @Override
    public CompletableFuture<SignatureHelp> signatureHelp(TextDocumentPositionParams position) {
        return null;
    }

    @Override
    public CompletableFuture<List<? extends Location>> definition(TextDocumentPositionParams position) {
       /* return CompletableFuture.supplyAsync(() -> {
            DocumentModel doc = docs.get(position.getTextDocument().getUri());
            String variable = doc.getVariable(position.getPosition().getLine(), position.getPosition().getCharacter());
            if (variable != null) {
                int variableLine = doc.getDefintionLine(variable);
                if (variableLine == -1) {
                    return Collections.emptyList();
                }
                Location location = new Location(position.getTextDocument().getUri(), new Range(
                        new Position(variableLine, 0),
                        new Position(variableLine, variable.length())
                ));
                return Collections.singletonList(location);
            }
            return null;
        });*/
       return null;
    }

    @Override
    public CompletableFuture<List<? extends Location>> references(ReferenceParams params) {
        /*return CompletableFuture.supplyAsync(() -> {
            DocumentModel doc = docs.get(params.getTextDocument().getUri());
            String variable = doc.getVariable(params.getPosition().getLine(), params.getPosition().getCharacter());
            if (variable != null) {
                return doc.getResolvedRoutes().stream()
                        .filter(route -> route.text.contains("${" + variable + "}") || route.text.startsWith(variable + "="))
                        .map(route -> new Location(params.getTextDocument().getUri(), new Range(
                                new Position(route.line, route.text.indexOf(variable)),
                                new Position(route.line, route.text.indexOf(variable) + variable.length())
                        )))
                        .collect(Collectors.toList());
            }
            String routeName = doc.getResolvedRoutes().stream()
                    .filter(route -> route.line == params.getPosition().getLine())
                    .collect(Collectors.toList())
                    .get(0)
                    .name;
            return doc.getResolvedRoutes().stream()
                    .filter(route -> route.name.equals(routeName))
                    .map(route -> new Location(params.getTextDocument().getUri(), new Range(
                            new Position(route.line, 0),
                            new Position(route.line, route.text.length()))))
                    .collect(Collectors.toList());
        });*/
        return null;
    }

    @Override
    public CompletableFuture<List<? extends DocumentHighlight>> documentHighlight(TextDocumentPositionParams position) {
        return null;
    }

    @Override
    public CompletableFuture<List<? extends SymbolInformation>> documentSymbol(DocumentSymbolParams params) {
      /*DocumentModel model = docs.get(params.getTextDocument().getUri());
        if(model == null)
            return null;
        return CompletableFuture.supplyAsync(() ->
                docs.get(params.getTextDocument().getUri()).getResolvedLines().stream().map(line -> {
                    SymbolInformation symbol = new SymbolInformation();
                    symbol.setLocation(new Location(params.getTextDocument().getUri(), new Range(
                            new Position(line.line, line.charOffset),
                            new Position(line.line, line.charOffset + line.text.length()))));
                    if (line instanceof VariableDefinition) {
                        symbol.setKind(SymbolKind.Variable);
                        symbol.setName(((VariableDefinition) line).variableName);
                    } else if (line instanceof Route) {
                        symbol.setKind(SymbolKind.String);
                        symbol.setName(((Route) line).name);
                    }
                    return symbol;
                }).collect(Collectors.toList())
        );*/
      return null;
    }

    @Override
    public CompletableFuture<List<? extends Command>> codeAction(CodeActionParams params) {
        return null;
    }

    @Override
    public CompletableFuture<List<? extends CodeLens>> codeLens(CodeLensParams params) {
        return null;
    }

    @Override
    public CompletableFuture<CodeLens> resolveCodeLens(CodeLens unresolved) {
        return null;
    }

    @Override
    public CompletableFuture<List<? extends TextEdit>> formatting(DocumentFormattingParams params) {
        return null;
    }

    @Override
    public CompletableFuture<List<? extends TextEdit>> rangeFormatting(DocumentRangeFormattingParams params) {
        return null;
    }

    @Override
    public CompletableFuture<List<? extends TextEdit>> onTypeFormatting(DocumentOnTypeFormattingParams params) {
        return null;
    }

    @Override
    public CompletableFuture<WorkspaceEdit> rename(RenameParams params) {
        return null;
    }

    @Override
    public void didOpen(DidOpenTextDocumentParams params) {
        DocumentModel model = new DocumentModel(params.getTextDocument().getText());
        this.docs.put(params.getTextDocument().getUri(),
                model);
      /*  CompletableFuture.runAsync(() ->
                javaLanguageServer.client.publishDiagnostics(
                        new PublishDiagnosticsParams(params.getTextDocument().getUri(), validate(model))
                )
        );*/
    }

    @Override
    public void didChange(DidChangeTextDocumentParams params) {
       DocumentModel model = new DocumentModel(params.getContentChanges().get(0).getText());
       this.docs.put(params.getTextDocument().getUri(),
                model);
        // send notification
        CompletableFuture.runAsync(() ->
                javaLanguageServer.client.publishDiagnostics(
                        new PublishDiagnosticsParams(params.getTextDocument().getUri(), validate(model))
                )
        );
    }
    private List<Diagnostic> validate(DocumentModel model) {
        /*System.err.println("validate");
        List<Diagnostic> res = new ArrayList<>();
        Route previousRoute = null;
        for (Route route : model.getResolvedRoutes()) {
            if (!EclipseConMap.INSTANCE.all.contains(route.name)) {
                Diagnostic diagnostic = new Diagnostic();
                diagnostic.setSeverity(DiagnosticSeverity.Error);
                diagnostic.setMessage("This is not a Session");
                diagnostic.setRange(new Range(
                        new Position(route.line, route.charOffset),
                        new Position(route.line, route.charOffset + route.text.length())));
                res.add(diagnostic);
            } else if (previousRoute != null && !EclipseConMap.INSTANCE.startsFrom(route.name, previousRoute.name)) {
                Diagnostic diagnostic = new Diagnostic();
                diagnostic.setSeverity(DiagnosticSeverity.Warning);
                diagnostic.setMessage("'" + route.name + "' does not follow '" + previousRoute.name + "'");
                diagnostic.setRange(new Range(
                        new Position(route.line, route.charOffset),
                        new Position(route.line, route.charOffset + route.text.length())));
                res.add(diagnostic);
            }
            previousRoute = route;
        }
        System.err.println("validate end");
        return res;
*/
        return null;
    }
    @Override
    public void didClose(DidCloseTextDocumentParams params) {
        this.docs.remove(params.getTextDocument().getUri());
    }

    @Override
    public void didSave(DidSaveTextDocumentParams params) {

        File file = new File(params.getTextDocument().getUri());
        JavaLanguageServerCompiler comp = new JavaLanguageServerCompiler();
        comp.run(new String[]{file.getPath().replace("file:","")});
        if (comp.hasErrors()){
            List<Diagnostic> diagList = new ArrayList<>();
            for (Problem e : comp.getErrors()) {
                Diagnostic diag = new Diagnostic();
                diag.setSeverity(DiagnosticSeverity.Error);
                diag.setMessage(e.message());
                diag.setRange(new Range(
                        new Position(e.line()-1, e.column()-1),
                        new Position(e.line()-1, e.endColumn())));
                diagList.add(diag);
            }
            CompletableFuture.runAsync(() ->
                    javaLanguageServer.client.publishDiagnostics(
                            new PublishDiagnosticsParams(params.getTextDocument().getUri(), diagList)));
        }

    }

}