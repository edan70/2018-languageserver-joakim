package cs.lth.langserv;


import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import org.eclipse.lsp4j.*;
import org.eclipse.lsp4j.services.*;


public class JavaLanguageServer implements LanguageServer {
	private TextDocumentService textService;
	private WorkspaceService workspaceService;
	LanguageClient client;

	public JavaLanguageServer() {

		textService = new TemplateTextDocumentService(this);
		workspaceService = new TemplateWorkspaceService();
	}
/**
	 * The initialize request is sent as the first request from the client to
	 * the server.
	 * 
	 * If the server receives request or notification before the initialize request it should act as follows:
	 * 	- for a request the respond should be errored with code: -32001. The message can be picked by the server.
	 *  - notifications should be dropped, except for the exit notification. This will allow the exit a server without an initialize request.
	 *  
	 * Until the server has responded to the initialize request with an InitializeResult 
	 * the client must not sent any additional requests or notifications to the server.
	 * 
	 * During the initialize request the server is allowed to sent the notifications window/showMessage, 
	 * window/logMessage and telemetry/event as well as the window/showMessageRequest request to the client.
	 */
	@Override
	public CompletableFuture<InitializeResult> initialize(InitializeParams params) {
		final InitializeResult res = new InitializeResult(new ServerCapabilities());
		res.getCapabilities().setCodeActionProvider(Boolean.FALSE);
		ArrayList<String> dummy = new ArrayList<>();
		dummy.add(".");
		res.getCapabilities().setCompletionProvider(new CompletionOptions(true, dummy));
		res.getCapabilities().setDefinitionProvider(Boolean.FALSE);
		res.getCapabilities().setHoverProvider(Boolean.TRUE);
		res.getCapabilities().setReferencesProvider(Boolean.FALSE);
		res.getCapabilities().setTextDocumentSync(TextDocumentSyncKind.Full);
		res.getCapabilities().setDocumentSymbolProvider(Boolean.FALSE);

		return CompletableFuture.supplyAsync(() -> res);
	}

	/**
	 * The initialized notification is sent from the client to the server after
	 * the client received the result of the initialize request but before the
	 * client is sending any other request or notification to the server. The
	 * server can use the initialized notification for example to dynamically
	 * register capabilities.
	 */
	@Override
	public void initialized(InitializedParams params) {
		initialized();
	}


	/**
	 * The shutdown request is sent from the client to the server. It asks the
	 * server to shutdown, but to not exit (otherwise the response might not be
	 * delivered correctly to the client). There is a separate exit notification
	 * that asks the server to exit.
	 */
	@Override
	public CompletableFuture<Object> shutdown(){
		return CompletableFuture.completedFuture(null);
	}

	/**
	 * A notification to ask the server to exit its process.
	 */
	@Override
	public void exit(){
	}

	/**
	 * Provides access to the textDocument services.
	 */
	@Override
	public TextDocumentService getTextDocumentService(){
		return textService;

	}

	/**
	 * Provides access to the workspace services.
	 */
	@Override
	public WorkspaceService getWorkspaceService() {
		return workspaceService;

	}
	public void setRemoteProxy(LanguageClient remoteProxy) {
		this.client = remoteProxy;
	}
}
