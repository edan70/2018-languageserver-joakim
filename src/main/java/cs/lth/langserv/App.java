package cs.lth.langserv;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.eclipse.lsp4j.jsonrpc.Launcher;
import org.eclipse.lsp4j.launch.LSPLauncher;
import org.eclipse.lsp4j.services.LanguageClient;

import org.extendj.JavaCompiler;
/**
 * Hello world!
 *
 */
public class App 
{
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		//JavaCompiler comp = new JavaCompiler();
		//File file = new File("/home/kobiya/Small.java");
		//comp.run(new String[] {file.getPath()});
		startServer(System.in, System.out);

	}

	public static void startServer(InputStream in, OutputStream out) throws InterruptedException, ExecutionException {
		JavaLanguageServer server = new JavaLanguageServer();
		System.err.println("Hello World!");
		Launcher<LanguageClient> l = LSPLauncher.createServerLauncher(server, in, out);
		System.err.println("Hello World!");
		Future<?> startListening = l.startListening();
		System.err.println("Hello World!");
		server.setRemoteProxy(l.getRemoteProxy());
		System.err.println("Hello World!");
		startListening.get();
		System.err.println("Hello World!");
	}
}
