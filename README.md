Java Language Server using ExtendJ
==================================

License
---

This repository is covered by the license BSD 2-clause, see file LICENSE.



Running:
--

Right now the server works if you compile it with mvn package
And then run with java -cp target/langserv-1.0-SNAPSHOT-jar-with-dependencies.jar cs.lth.langserv.App

The current implementation shows some information about line and column if you hover over any text in sublime.
Also if you save the file, the language server will compile your java file
in the background and publish any error messages to the client.
Currently this shows up as a pop-up in sublime. The intention is that this
should be published as diagnostics, and as such the editor should
handle it better in the future.

Currently it is running using stdin and stdout for communication. This appears to be one of the ways 
it is usually implemented. Thus you need to setup the editor
you want to use to actually run the program.

If you have the LSP plugin installed in sublime, found at:


https://github.com/tomv564/LSP


You should add:
```
{
	"clients":
	{
		"test":
		{
			"command":
			[	"java",
			    "-cp",
			    "/PATH-TO-LOCAL-REPOSITORY/target/langserv-1.0-SNAPSHOT-jar-with-dependencies.jar:/PATH-TO-LOCAL-REPOSITORY/lib/extendj.jar",
			     "cs.lth.langserv.App"
			],
			"enabled": true,
			"languageId": "java",
			"scopes":
			[
				"source.java"
			],
			"syntaxes":
			[
				"Packages/Java/Java.sublime-syntax"
			],
			//"tcp_port": 8080
		}
	}
}
```

in the file: LSP.sublime-settings, this is accessible if you go to settings for LSP in sublime


Where "PATH-TO-LOCAL-REPOSITORY is the absolute path to where this repository was downloaded on your computer.

I have also tested the server with Neovim, using the language client found at:

https://github.com/autozimu/LanguageClient-neovim

If you follow the steps for installation, and then add: 
```
" Required for operations modifying multiple buffers like rename.
set hidden

let g:LanguageClient_serverCommands = {
    \ 'java': ['java', '-cp', '/home/kobiya/langserv/langserv/langserv/target/langserv-1.0-SNAPSHOT-jar-with-dependencies.jar:/home/kobiya/langserv/langserv/langserv/lib/extendj.jar', 'cs.lth.langserv.App'],
    \ }

set runtimepath+=~/.vim-plugins/LanguageClient-neovim


nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>
```
In nevoim's init file. Found at ~/config/nevoim/init.vim as default.


Credits
-------
The following files in the repository come from:


From https://github.com/LucasBullen/LSP4J_Tutorial/ the following files are either directly used or modified to suit my purposes:

* DocumentModel
* EclipseConMap  (This was added to make sure things compiled earlier, will probably be deleted in the near future)
* JavaLanguageServer
* TemplateTextDocumentService (renamed from EclipseConTextDocumentService in the exercies)
* TemplateWorkspaceService	 (renamed from EclipseConWorkspaceService in the exercies)


---
The jar file in the lib directory is the compiled ExtendJ compiler found at:


https://bitbucket.org/extendj/extendj/src/master/


The class "JavaLanguageServerCompiler" is a modified version of the "JavaCompiler" class
from the ExtendJ project. To make the errors visible to me.
